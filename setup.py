"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
from setuptools import setup, find_packages

setup(
    name="salvus_data_sets",
    version="0.0.1",
    author="Mondaic AG",
    author_email="info@mondaic.com",
    classifiers=["Programming Language :: Python :: 3"],
    packages=find_packages(),
    python_requires=">=3.7, <4",
    install_requires=[
        "numpy",
        "scipy",
        "matplotlib",
        "netCDF4",
        "pooch",
        "click",
        "tqdm",
        "xarray",
        "obspy>=1.2.0",
    ],
    entry_points={  # Optional
        "console_scripts": ["salvus-data-sets=salvus_data_sets.cli:main"],
    },
)
