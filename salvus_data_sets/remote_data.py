"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import typing

import pooch
from pooch import HTTPDownloader, Untar, Decompress


SALVUS_REMOTE_DATA = pooch.create(
    path=pooch.os_cache("salvus_data_sets"),
    base_url="https://data.mondaic.com",
    registry={
        "GCMT/jan76_dec17.ndk.gz": "28bf6dbbe65ecd1cfcc63076f88e7027023e821ff3f6e64f6ccd333721e7c1bb",  # NOQA
        # 2018
        "GCMT/2018/jan18.ndk": "7ae0203ae5bacdb33c6e4571ca11cef0bfa4d53f0344fe8d0be24f852ffc48e2",
        "GCMT/2018/feb18.ndk": "c72cb57cbd10086c4b165f4f8a9b36763f25048d83039a5dcd759fa3bfc45ba3",
        "GCMT/2018/mar18.ndk": "a7e8d418b5b8a37a91e185d42a2794d1006759f2791fc2154f84bcada57a6810",
        "GCMT/2018/apr18.ndk": "54fb19190462f1cf1573e0f3be8bc1c9059f065bfe3ba5009d3fefc2d5ea95f8",
        "GCMT/2018/may18.ndk": "08ec216893cbc1c1e1f7bd6cf65ac30a3b470a90d02ad19302a2ca2f946ff975",
        "GCMT/2018/jun18.ndk": "e621ddc8670c77430ba22d7126bd4f23bc050aa8b9a44d9d1145db60ac54f9c8",
        "GCMT/2018/jul18.ndk": "91c7fe27c8302b5ce6f0dbc0adbc58b5ff9bd384448f10336bce63db0993f113",
        "GCMT/2018/aug18.ndk": "309fca68e7c2799a2e6424943d602ae8cc44983c4f6ab8a1a6c6f7b8b8139835",
        "GCMT/2018/sep18.ndk": "7c8deda5552a3f29721b27a8f9c0e0be2fd81e33360748dfb38a1c96f76e9531",
        "GCMT/2018/oct18.ndk": "cdb248d224f124895ab62bf8e39580dd502b989e91336de15461ebefb9bb8881",
        "GCMT/2018/nov18.ndk": "b03ddb7a0e3d9b4945c96cd369b178be7dc7859fc737b066ad4ca72da3671da0",
        "GCMT/2018/dec18.ndk": "a6ab43a833488c6ac9e62ceccb460689f60aaa03ac09b475a2ae8e9ddd363c62",
        # 2019
        "GCMT/2019/jan19.ndk": "05614a91861abe168d229d62f05a5c86ddc9ee62d73910b4ca54944d8a003a29",
        "GCMT/2019/feb19.ndk": "17fc918c07e31b9cce929f6b8fb775c76d1b2f3dc583aa2429ad74b1442f7709",
        "GCMT/2019/mar19.ndk": "832e293c9e910bebb61bed02a0eed985fd2133dbfd2dfa1a6ff75c7a7e24dc88",
        "GCMT/2019/apr19.ndk": "823e000bd5e49dbe482bdd84525e8ddf39062fe0b8cb732b3bbd31631186facd",
        "GCMT/2019/may19.ndk": "dce59693cde3f0653a1dd4758eba1cc80eb2eef6aa3fbd8bd7fe63d52d6dfa1c",
        "GCMT/2019/jun19.ndk": "abc49e98b4ad13a7e8cea60c356ce139f69cb71d01597f5d1bf9a9b90b93de40",
        "GCMT/2019/jul19.ndk": "86514f1dba420d69a3f0257757267942b08a6b80f8cdcd6cf7c935620ba5fbba",
        "GCMT/2019/aug19.ndk": "873d00a9e2334ecf9374c300351f2e4273ae4a24ebe585b2beeb7f96c9c5b7a6",
        "GCMT/2019/sep19.ndk": "a3c7208e888b657bd6550d115aa1f3069167f9064e981f1776f6166576e1ea74",
        "GCMT/2019/oct19.ndk": "abd4e53ea6fad55ce8570e175c2b4a72508f1d3e61a626a73f9f60021f7e8e15",
        "GCMT/2019/nov19.ndk": "cdbedc451d26a62197cb9586e3dea6f0f6c9898d52758760d59d2f1736be9a1a",
        "GCMT/2019/dec19.ndk": "062df3a7acb438feb6e743a3e53df72f571b5a8dd7608752af54c381dd19dd60",
        # 2020
        "GCMT/2020/jan20.ndk": "8975f83b0463598a380749d444286618fad629102a375be7457a9f90a5ed0360",
        "GCMT/2020/feb20.ndk": "681c246ba58f6d921b6c2c2f5d35ba3b49aa227f75cf3c6dea233126d08826f4",
        "GCMT/2020/mar20.ndk": "2c7189716857e39a6ad4ed6e76f0901573579a5a2338aaa285bb1b583af0bc07",
        "GCMT/2020/apr20.ndk": "5cd12cdb04ace0655bfca09310be3843bbb526a6e03d3c6cf442249d4308dfcc",
        "GCMT/2020/may20.ndk": "4ca0bacf2bb46a9780ecab4f670a941428cf5bbed6329b1e249355d4512ae08d",
        "GCMT/2020/jun20.ndk": "2a785c62dd6d100a3ddb6033cf57bace78ab58a7b40f206b478aaae8d5ea73d1",
        "GCMT/2020/jul20.ndk": "d4d2f2c0cf2b34dc5b599b5511a787bb9a1d409219a41171b1580d1727ec6f4e",
        "GCMT/2020/aug20.ndk": "0374d08cd473ef2d4441064b1d63f098ebc585350ea1ab04b46ed87dd5c51304",
        "GCMT/2020/sep20.ndk": "0120fdc0dc6e1306881cb5c9ff1a6ce48558f97a9efd6e1ec6ddcb26da099f6f",
        "GCMT/2020/oct20.ndk": "0b932c00460a33396e422f34daeb0d7e949f87bc149853b5516684e278a4687e",
        "GCMT/2020/nov20.ndk": "f62f36b91a9df56fb3744d66e3060065970af353542e44ace5387ade3953b890",
        "GCMT/2020/dec20.ndk": "51e82961e9ec5aee258294b1b8e3082afc59ba14eea7890ec0fa999b8921a97e",
        # 2021
        "GCMT/2021/jan21.ndk": "c9c5977b7cffa904304ba80571f9c7f0e747ee349e3fefd96393629e44e8f3cb",
        "GCMT/2021/feb21.ndk": "ab12992d59c174922cbf4fb789f28671cf30a2d0734b03fa499ff2e3a85e171e",
        "GCMT/2021/mar21.ndk": "bc898cb8b108f9dd016be18e5437c82a5a98ae023174224dd96ca7b1fe891b2c",
        # Not available as of July 2021.
        #"GCMT/2021/apr21.ndk": "",
        #"GCMT/2021/may21.ndk": "",
        #"GCMT/2021/jun21.ndk": "",
        #"GCMT/2021/jul21.ndk": "",
        #"GCMT/2021/aug21.ndk": "",
        #"GCMT/2021/sep21.ndk": "",
        #"GCMT/2021/oct21.ndk": "",
        #"GCMT/2021/nov21.ndk": "",
        #"GCMT/2021/dec21.ndk": "",
    },
    # All are custom URLS in the end ...
    urls={
        # Everything up to Dez 2017.
        "GCMT/jan76_dec17.ndk.gz": "http://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/jan76_dec17.ndk.gz",  # NOQA
        # 2018.
        "GCMT/2018/jan18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/jan18.ndk",  # NOQA
        "GCMT/2018/feb18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/feb18.ndk",  # NOQA
        "GCMT/2018/mar18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/mar18.ndk",  # NOQA
        "GCMT/2018/apr18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/apr18.ndk",  # NOQA
        "GCMT/2018/may18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/may18.ndk",  # NOQA
        "GCMT/2018/jun18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/jun18.ndk",  # NOQA
        "GCMT/2018/jul18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/jul18.ndk",  # NOQA
        "GCMT/2018/aug18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/aug18.ndk",  # NOQA
        "GCMT/2018/sep18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/sep18.ndk",  # NOQA
        "GCMT/2018/oct18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/oct18.ndk",  # NOQA
        "GCMT/2018/nov18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/nov18.ndk",  # NOQA
        "GCMT/2018/dec18.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2018/dec18.ndk",  # NOQA
        # 2019.
        "GCMT/2019/jan19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/jan19.ndk",  # NOQA
        "GCMT/2019/feb19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/feb19.ndk",  # NOQA
        "GCMT/2019/mar19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/mar19.ndk",  # NOQA
        "GCMT/2019/apr19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/apr19.ndk",  # NOQA
        "GCMT/2019/may19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/may19.ndk",  # NOQA
        "GCMT/2019/jun19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/jun19.ndk",  # NOQA
        "GCMT/2019/jul19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/jul19.ndk",  # NOQA
        "GCMT/2019/aug19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/aug19.ndk",  # NOQA
        "GCMT/2019/sep19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/sep19.ndk",  # NOQA
        "GCMT/2019/oct19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/oct19.ndk",  # NOQA
        "GCMT/2019/nov19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/nov19.ndk",  # NOQA
        "GCMT/2019/dec19.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2019/dec19.ndk",  # NOQA
        # 2020.
        "GCMT/2020/jan20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/jan20.ndk",  # NOQA
        "GCMT/2020/feb20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/feb20.ndk",  # NOQA
        "GCMT/2020/mar20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/mar20.ndk",  # NOQA
        "GCMT/2020/apr20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/apr20.ndk",  # NOQA
        "GCMT/2020/may20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/may20.ndk",  # NOQA
        "GCMT/2020/jun20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/jun20.ndk",  # NOQA
        "GCMT/2020/jul20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/jul20.ndk",  # NOQA
        "GCMT/2020/aug20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/aug20.ndk",  # NOQA
        "GCMT/2020/sep20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/sep20.ndk",  # NOQA
        "GCMT/2020/oct20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/oct20.ndk",  # NOQA
        "GCMT/2020/nov20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/nov20.ndk",  # NOQA
        "GCMT/2020/dec20.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2020/dec20.ndk",  # NOQA
        # 2021.
        "GCMT/2021/jan21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/jan21.ndk",  # NOQA
        "GCMT/2021/feb21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/feb21.ndk",  # NOQA
        "GCMT/2021/mar21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/mar21.ndk",  # NOQA
        "GCMT/2021/apr21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/apr21.ndk",  # NOQA
        "GCMT/2021/may21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/may21.ndk",  # NOQA
        "GCMT/2021/jun21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/jun21.ndk",  # NOQA
        "GCMT/2021/jul21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/jul21.ndk",  # NOQA
        "GCMT/2021/aug21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/aug21.ndk",  # NOQA
        "GCMT/2021/sep21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/sep21.ndk",  # NOQA
        "GCMT/2021/oct21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/oct21.ndk",  # NOQA
        "GCMT/2021/nov21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/nov21.ndk",  # NOQA
        "GCMT/2021/dec21.ndk": "https://www.ldeo.columbia.edu/~gcmt/projects/CMT/catalog/NEW_MONTHLY/2021/dec21.ndk",  # NOQA
    },
)


def fetch(key: str) -> pathlib.Path:
    downloader = HTTPDownloader(progressbar=True)
    processor = None
    if key.endswith("tar.gz"):
        processor = Untar()
    elif key.endswith(".gz"):
        processor = Decompress()
    fname = SALVUS_REMOTE_DATA.fetch(
        key, downloader=downloader, processor=processor
    )

    return pathlib.Path(fname)
