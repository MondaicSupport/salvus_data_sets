# Salvus*DataSets*

A utility library to create data sets for use inside Salvus.

## Installation

We recommend a separate `conda` environment. Then just run

```shell
$ pip install -v -e .
```

## Usage

### GCMT Catalog as a CSV File

This command will download and compress the whole GCMT catalog into a single
CSV file that can be read by `pandas` and is less than 2 MB in size.

```shell
$ salvus-data-sets build-gcmt-catalog-file --output-filename=gcmt_catalog.csv.bz2
```
